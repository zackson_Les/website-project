import { Component, OnInit } from '@angular/core';
import { generate } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.css']
})
export class PhotosComponent implements OnInit {
  page = "<style>img{width:170px;}	div.img:hover {transform: scale(1.2,1.2);}</style>"
      
  +"<table>\n";
  generateRan(max,min){
    var random = [];
    for(var i = min;i<max ; i++){
        var temp = Math.floor(Math.random()*max);
        if(random.indexOf(temp) == -1){
            random.push(temp);
        }
        else
         i--;
    }
    return random;
    //console.log(random)
}
a = 1
img: FormGroup;

  onSubmit(){
    var page = "<style>img{width:150px;}	div.img:hover {transform: scale(1.2,1.2);}</style>"+"<table>\n";;
    var old_a =this.a;
    this.a++;
        page += "<td>"
        var imgs = 28*this.a
        if(imgs > 86){
          this.a = 1;
          imgs = 28;
          old_a = 0;
        }
             
            var min = 28*old_a;
           // alert(imgs + " aa " + min + " bb " + this.a +" "+ old_a);
            var imgs = 28*this.a
            var im = this.generateRan(imgs,0);
            //alert(im);
            for(var i = min; i < imgs; i++){
             // alert(i);
             if((parseInt(im[i])+1) < 10){
                page += '<a href="assets/images/0'+(parseInt(im[i])+1)+'.jpg">\n<div class="img">\n<img src="assets/images/0'+(parseInt(im[i])+1)+'.jpg" id="img"/>\n</div>\n</a>';
            }
            else if((parseInt(im[i])+1) >= 10){
                page += '<a href="assets/images/'+(parseInt(im[i])+1)+'.jpg">\n<div class="img">\n<img src="assets/images/'+(parseInt(im[i])+1)+'.jpg" id="img"/>\n</div>\n</a>';
            }
            if((i+1) % 4 == 0 && i != 0){
                page += "</td>\n<td>"
            }
       }   
    page += "\n</td>\n</table>\n";
    var div=document.getElementById('m');
    div.innerHTML = page;
   // alert(page);
  }

  constructor() { 
    this.img = new FormGroup({
      next: new FormControl()
   });
    this.page += "<td>"
    var imgs = 28
    var im = this.generateRan(imgs,0);
    for(var i = 0; i < imgs; i++){
      
      if((parseInt(im[i])+1) < 10){
        this.page += '<a href="assets/images/0'+(parseInt(im[i])+1)+'.jpg">\n<div class="img">\n<img src="assets/images/0'+(parseInt(im[i])+1)+'.jpg" id="img"/>\n</div>\n</a>';
      }
      else if((parseInt(im[i])+1) >= 10){
        this.page += '<a href="assets/images/'+(parseInt(im[i])+1)+'.jpg">\n<div class="img">\n<img src="assets/images/'+(parseInt(im[i])+1)+'.jpg" id="img"/>\n</div>\n</a>';
      }
      if((i+1) % 4 == 0 && i != 0){
        this.page += "</td>\n<td>"
      }

    }
    this.page += "\n</td>\n</table>\n";
  }

  ngOnInit() {
    var div=document.getElementById('m');
    div.innerHTML = this.page;
    //document.write(this.page)
  }

}
