import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {saveAs} from 'file-saver';

@Component({
  selector: 'app-contect',
  templateUrl: './contect.component.html',
  styleUrls: ['./contect.component.css']
})
export class ContectComponent implements OnInit {
  submmit = false;

  succses = false;
  msg: FormGroup;
  constructor(private formBuilder: FormBuilder) { 
    this.msg = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['',Validators.required],
      phone: ['',Validators.required]
    })
  }

  onSubmit(){
    this.submmit = true;

    if(this.msg.invalid){
      return;
    }

    this.succses = true;

    var to_server = "/INF,"+this.msg.controls.name.value+","+this.msg.controls.email.value+","+this.msg.controls.phone.value;

  }

  ngOnInit() {

  }

}
